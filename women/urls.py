from django.urls import path, re_path
from django.views.decorators.cache import cache_page
from .views import *

urlpatterns = [
    path('', WemenHome.as_view(), name='home'),
    path('about/', about, name='about'),
    path('addarticle/', AddArticle.as_view(), name='add_article'),
    path('feedback/', FeedbackFormView.as_view(), name='feedback'),
    path('register/', RegisterUser.as_view(), name='register'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', logout_user, name='logout'),
    path('post/<slug:post_slug>/', ShowPost.as_view(), name='post'),
    path('category/<slug:category_slug>/', WomenCategory.as_view(), name='category')
    # path('categories/<int:catid>/', categories),
    # re_path(r'^archive/(?P<year>[0-9]{4})', archive),
]