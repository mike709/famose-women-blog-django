from django.db.models import Count
from django.core.cache import cache
from .models import *

menu = [
    {'title': 'About', 'url_name': "about"},
    {'title': 'Add article', 'url_name': "add_article"},
    {'title': 'Feedback', 'url_name': "feedback"},
]

class DataMixin():
    paginate_by = 3
    def get_user_context(self, **kwargs):
        context = kwargs
        category = cache.get('category')

        if not category:
            category = Category.objects.annotate(Count('women'))
            cache.set('category', category, 60)
            
        context['menu'] = menu
        context['category'] = category

        if 'category_selected' not in context:
            context['category_selected'] = 0
        return context