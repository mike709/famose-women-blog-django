from django import template
from women.models import *

register = template.Library()

@register.simple_tag(name='get_cats')
def get_categories(filter=None):
    if not filter:
        return Category.objects.all()
    else:
        return Category.objects.filter(pk=filter)
    
@register.inclusion_tag('list_categories.html')
def show_category(sort=None, category_selected=0):
    if not sort:
        categories = Category.objects.all()
    else:
        categories = Category.objects.order_by(sort)

    return {'cats': categories, 'category_selected': category_selected}