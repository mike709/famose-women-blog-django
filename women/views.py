from django.http import Http404, HttpResponse, HttpResponseNotFound
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from .forms import AddPostForm, RegisterUserForm, LoginUserForm, ContactForm
from django.views.generic.edit import FormView
from django.contrib.auth.views import LoginView
from django.contrib.auth import logout, login
from .utils import *
from .models import *


class WemenHome(DataMixin, ListView):
    model = Women
    template_name = 'index.html'
    context_object_name = 'posts'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_def = self.get_user_context(title="Main page")
        return dict(list(context.items()) + list(category_def.items()))

    def get_queryset(self):
        return Women.objects.filter(is_published=True).select_related('category')


# def index(request):
#     posts = Women.objects.all()
#     context = {
#         'posts': posts,
#         'menu': menu,
#         'title': 'Main Page',
#         'category_selected': 0
#     }
#     return render(request, 'index.html', context=context)


def about(request):
    contact_list = Women.objects.all()
    paginator = Paginator(contact_list, 3)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, 'about.html', {'title': 'About', 'menu': menu, 'page_obj': page_obj})


def categories(request, catid):
    if request.GET:
        print(request.GET)
    return HttpResponse(f"<h1>Articals by categories</h1><p>{catid}</p>")


def archive(request, year):
    if int(year) > 2022:
        return redirect('home')
    return HttpResponse(f"<h1>Archive by years</h1><p>{year}</p>")


def pageNotFound(request, exception):
    return HttpResponseNotFound("<h1>Page not found</h1>")


class AddArticle(LoginRequiredMixin, DataMixin, CreateView):
    form_class = AddPostForm
    template_name = 'add_article.html'
    success_url = reverse_lazy('home')
    login_url = reverse_lazy('home')
    raise_exception = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_def = self.get_user_context(title="Add article")
        return dict(list(context.items()) + list(category_def.items()))
# def add_article(request):
#     if request.method == 'POST':
#         form = AddPostForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#             return redirect('home')
#     else:
#         form = AddPostForm()

#     return render(request, 'add_article.html', {'form': form, 'menu': menu, 'title': 'Add article'})


# def feedback(request):
#     return HttpResponse("Feedback")

class FeedbackFormView(DataMixin, FormView):
    form_class = ContactForm
    template_name = 'feedback.html'
    success_url = reverse_lazy('home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_def = self.get_user_context(title="Feedback")
        return dict(list(context.items()) + list(category_def.items()))
    
    def form_valid(self, form):
        print(form.cleaned_data)
        return redirect('home')

# def login(request):
#     return HttpResponse("Login")


class RegisterUser(DataMixin, CreateView):
    form_class = RegisterUserForm
    template_name = 'register.html'
    success_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_def = self.get_user_context(title='Registration')
        return dict(list(context.items()) + list(category_def.items()))
    
    def form_valid(self, form) -> HttpResponse:
        user = form.save()
        login(self.request, user)
        return redirect('home')


class ShowPost(DataMixin, DetailView):
    model = Women
    template_name = 'post.html'
    slug_url_kwarg = 'post_slug'
    context_object_name = 'post'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_def = self.get_user_context(title=context["post"])
        # context['category_selected'] = context['post'][0].category_id
        return dict(list(context.items()) + list(category_def.items()))

# def show_post(request, post_slug):
#     post = get_object_or_404(Women, slug=post_slug)

#     context = {
#         'post': post,
#         'menu': menu,
#         'title': post.title,
#         'cat_selected': post.category_id
#     }

#     return render(request, 'post.html', context=context)


class WomenCategory(DataMixin, ListView):
    model = Women
    template_name = 'index.html'
    context_object_name = 'posts'
    allow_empty = False

    def get_queryset(self):
        return Women.objects.filter(category__slug=self.kwargs['category_slug'], is_published=True).select_related('category')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = Category.objects.get(slug=self.kwargs['category_slug'])
        category_def = self.get_user_context(title='Category - ' + str(category.name),
                                             category_selected=category.pk)
        return dict(list(context.items()) + list(category_def.items()))
        # def show_category(request, category_slug):
        #     categoryId = Category.objects.get(slug=category_slug)
        #     posts = Women.objects.filter(category_id=categoryId.pk)

        #     if (len(posts)) == 0:
        #         raise Http404()

        #     context = {
        #         'posts': posts,
        #         'menu': menu,
        #         'title': 'View by category',
        #         'category_selected': categoryId.pk
        #     }
        #     return render(request, 'index.html', context=context)

class LoginUser(DataMixin, LoginView):
    form_class = LoginUserForm
    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category_def = self.get_user_context(title='Authorization')
        return dict(list(context.items()) + list(category_def.items()))
    
    def get_success_url(self) -> str:
        return reverse_lazy('home')


def logout_user(request):
    logout(request)
    return redirect('login')